from sre_constants import JUMP
import networkx as nx
import random
import drawSvg
from fairychess import parse_fairy, FairyChessPiece, parse_text, ParseError

from matplotlib import cm
import matplotlib.colors as mcolors
import numpy as np
from collections import Counter
from itertools import cycle, islice, chain
from collections import namedtuple, deque
from functools import partial
from time import sleep, time
from streamingtool import stream
import paho.mqtt.client as mqtt 
import json


strs = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZᚡᚢᚤᚦᚨᚪᚬᚭᚯᚰᚱᚲᚳᚴᚷᚹᚺᚻᚼᚾᛀᛂᛃᛄᛅᛆᛇᛈᛉᛊᛋᛏᛑᛒᛓᛕᛖᛗᛘᛚᛝᛞᛟᛠᛢᛣᛤᛥᛦᛨᛩᛮᛯᛰ"



class System:
    sz = 70
    mxto = 8
    tileable = False
    def __init__(self, sz, fcp):
        self.g = nx.Graph()
        self.g.add_nodes_from([(x, y) for x in range(sz) for y in range(sz)])
        self.disallow_c(fcp)

        self.c0 = set(max(nx.find_cliques(self.g), key=len))
        
        
            

        
        self.cpos = 0,0
        self.timeout = 100
        self.fcpd = get_fcp_drawing(fcp, self.tileable)
        
    def optimise_color_pattern(self):
        best_fitness = None
        best_pattern = None
        for vx in range(1,9):
            for vy in range(1,9):
                for dy in range(16):
                    for ds in range(1,16):
                        try:
                            self.colors = {x: ((x[0]//vx)+ dy*(x[1]//vy))%ds for i, x in enumerate(self.g.nodes)}
                            self.cpos = vx-1,vy-1
                            self.reassign_colors()
                            fitness = (self.total_errors()) * (len(self.pos_colors())),len(self.pos_colors())
                            if best_fitness is None or fitness < best_fitness:
                                best_fitness = fitness
                                best_pattern = self.colors
                                yield self.get_current_state_d(phase="starting pattern")
                        except ZeroDivisionError:
                            pass
        self.colors = best_pattern
        
    def reassign_colors(self):
        order = [a for a, _ in Counter(self.colors.values()).most_common()]
        map_ = {x:i for i,x in enumerate(order)}
        
        self.colors = {k:map_[v] for k,v in self.colors.items()}
        
        
        

    def disallow_c(self, fcp):
        
        for p1 in self.g.nodes:
            mvs = fcp.moves(*p1)
            if self.tileable:
                mvs= {(x%8, y%8) for x,y in mvs}
            for p2 in self.g.nodes:
                if p2 in mvs:
                    self.g.add_edge(p1, p2)

    @classmethod
    def error_display(cls):
        d = drawSvg.Drawing(8 * cls.sz, 8 * cls.sz)
        d.append(drawSvg.Rectangle(0, 0, 8 *cls.sz, 8 *cls.sz, fill="grey"))
        d.append(drawSvg.Text(
                        "ERROR",
                        50,
                        4 * cls.sz, 4 * cls.sz,
                        fill="red",
                        center=True,
                        valign="middle",
                    ))
        return d
        
        

    def display(self):
        sz = self.sz
        mx = 16
        #print(mcolors.CSS4_COLORS)
        colors = list(cm.get_cmap("rainbow")(np.linspace(0,1,max(self.pos_colors())+1))*255)
        
        colors = ["#{:02x}{:02x}{:02x}".format(*map(int,x)) for x in colors]
        real_colors = {k: colors[v] for k, v in self.colors.items()}
        d = drawSvg.Drawing(8 * sz, 8 * sz)

        for i in range(8):
            for j in range(8):
                
                
                d.append(
                    drawSvg.Rectangle(i * sz, j * sz, sz, sz, fill=real_colors[i, j])
                )
                
        i,j = self.cpos
                    
        d.append(
            drawSvg.Rectangle(i * sz, j * sz, sz, sz, stroke='black', stroke_width=5, fill=real_colors[i, j])
        )
        
        for i in range(8):
            for j in range(8):
                for k in range(self.errors((i,j))):
                    S = 12
                    sz2 = sz/(S+2)
                    kx, ky = k//S, k%S
                    d.append(
                        drawSvg.Circle(i * sz + (kx+1)*sz2, j * sz + (ky + 2)*sz2, sz2/2, fill='black')
                    )
                if (i,j) in self.c0:
                    sz2 = sz/12
                    d.append(drawSvg.Circle(i * sz + 10*sz2, j * sz + 10*sz2, sz2/2, stroke_width=sz2/4, stroke='black', fill=real_colors[i, j]))
                    d.append(
                            drawSvg.Rectangle(i * sz + 9*sz2, j * sz + 8*sz2, sz2*2, sz2*2, fill='black')
                        )
                        
                d.append(
                    drawSvg.Text(
                        strs[self.colors[i, j]],
                        sz//2,
                        i * sz + sz // 2,
                        j * sz + sz // 2,
                        fill="black",
                        center=True,
                        valign="middle",
                    )
                )

                
        return d
    
    def pos_colors(self):
        return set(self.colors.values())
    
    def errors(self, p):
        return sum(1 for x in self.g.neighbors(p) if self.colors[p]==self.colors[x])
    
    def total_errors(self):
        return sum(self.errors(x) for x in self.g.nodes)
    
    def improvement(self, p, c):
        return self.errors(p) - sum(1 for x in self.g.neighbors(p) if self.colors[x]==c)
    
    def pos_moves(self):
        for p in self.g.nodes:
            for c in self.pos_colors():
                i = self.improvement(p,c)
                if i >= 0:
                    if self.colors[p] != c:
                        if i or (bool(self.colors[p] < c) == bool(self.timeout % 2)):
                            yield (self.g.has_edge(p, self.cpos), i, -c), p,c
                    
    

    def improve(self):
        pmoves = list(self.pos_moves())
        pmoves.sort()
        if pmoves:
            _, p, v = pmoves[-1]
            self.colors[p] = v
        else:
            ncol = max(self.pos_colors()) + 1
            p = random.choices([x for x in self.g.nodes() if self.errors(x)> 0], k=1)[0]
            self.colors[p] = ncol
        self.cpos = p
        return True

    def improve_inf(self):
        
        tr = self.mxto
        self.timeout = tr
        
        while True:
            if self.total_errors() == 0 and len(set(self.colors.values())) == len(self.c0):
                break
            if self.total_errors() == 0 and not list(self.pos_moves()):
                self.timeout -= 1
                self.reassign_colors()
                if self.timeout < 0:
                    break
            q = len(self.pos_colors())
            try:
                self.improve()
            except (ValueError, IndexError, AssertionError):
                self.timeout -= 1
                if self.timeout < 0:
                    break
            else:
                if tr > self.timeout:
                    tr -= 1
                self.timeout = tr
                if q != len(self.pos_colors()):
                    tr = self.mxto

            yield self.get_current_state_d(phase="reducing colors " + ["down", "up"][self.timeout%2] if self.total_errors() == 0 else "breaking links")
            
    def improve_phases(self):
        yield from self.optimise_color_pattern()
        yield from self.improve_inf()
        if len(set(self.colors.values())) == len(self.c0):
            yield self.get_current_state_d(phase="theoretically optimal solution found")
        else:
            yield self.get_current_state_d(phase=f"locally optimal solution found ({self.mxto} times)")
    
    def get_current_state_d(self, **kwargs):
        kwargs.update(dict(
            fcpd = self.fcpd.asSvg(),
            disp = self.display().asSvg(),
            heuristic = self.total_errors(),
            colornum = len(set(self.colors.values())),
            tcolornum = len(self.c0),
            pos = self.cpos,
            timeout= self.timeout,
            loss= (self.total_errors()//2, sum(self.colors.values())),
        ))
        return kwargs
    
    @classmethod
    def get_invalid_state_d(cls, **kwargs):
        kwargs.update(dict(
            fcpd = cls.error_display().asSvg(),
            disp = cls.error_display().asSvg(),
            heuristic = 0,
            colornum = 0,
            tcolornum = 0,
            pos = (0,0),
            timeout= 0,
            loss= 0,
        ))
        return kwargs

def rgb(s):
    v = int(s[1:], 16)
    return (v//(256**2))%256, (v//256)%256, v%256


def get_fcp_drawing(fcp,tileable):
    
    sz = System.sz/2*16/17
    d = drawSvg.Drawing(17 * sz, 17 * sz)
    
    mvs = fcp.mvs 
        
    for i in range(-8, 9):
        for j in range(-8, 9):
            if tileable:
                i2,j2 = i%8, j%8
                im, jm = (-i)%8, (-j)%8
            else:
                i2,j2 = i, j
                im,jm = -i, -j
                
            col = (
                "blue"
                if (i, j) == (0, 0)
                else "green"
                if (i2, j2) == (0, 0)
                else "red"
                if (i2, jm) in mvs
                else "orange"
                if (im, j2) in mvs
                else "grey"
                if (i + j) % 2
                else "white"
            )
            d.append(drawSvg.Rectangle((i + 8) * sz, (j + 8) * sz, sz, sz, fill=col))

    return d


def get_next_q():
    yield from Q
    yield from (dict(user="auto", code=x) for x in V)


def check_fairy_valid(code):
    try:
        parse_fairy(code)
    except (ParseError, AssertionError):
        return False
    else:
        return True 

def get_system_ev(code):
    try:
        fcp = parse_fairy(code)
    except (ParseError, AssertionError):
        for i in range(40):
            x = System.get_invalid_state_d(
                q=list(islice(get_next_q(), 5)),
                phase="invalid code",
                code =code,
                text='///',
                countdown=40-i,
            )
            yield x
        return
    system = System(8, fcp )
    now = time()
    for x in system.improve_phases():
        try:
            text = parse_text(code)
        except ParseError:
            text= f"?invalid code: {code}?"
        x.update(
            code =code,
            text=text,
            q=list(islice(get_next_q(), 5)),
            countdown=0, 
        )
        #sleep(max(0, now - time() + 0.1))
        now = time()
        yield x
    for i in range(40):
        x.update(
            q=list(islice(get_next_q(), 5)),
            countdown=40-i,
        )
        yield x
        
        
Q = deque()
V = deque()
all_ = []

def loop():
    try:
        with open("states.json") as file:
            all_[:] = json.load(file)
    except FileNotFoundError:
        all_[:] = ['B-W','R-F','R-N','B-N','G-K','F6','F5','F4','F3', 'F2', 'F','W6','W5','W4','W3', 'W2','W', 'R','K','fW','B','BN','!(BN)', 'fRbB','Q', 'RN+','N+Z+C+','ZC','NZC','N-N', "N-N-N",'N-N-N-N',]
        with open("states.json", "w") as file:
            json.dump(all_, file)
        
    
            
    
    while True:
        if len(V) < len(all_):
            random.shuffle(all_)
            V.extend(all_.copy())
        
        if len(Q):
            yield from get_system_ev(Q.popleft()["code"])
        elif len(V):
            yield from get_system_ev(V.popleft())
        else:
            assert False
            
            

                    
            
            


def on_message(client, userdata, message):
    message = str(message.payload.decode("utf-8"))
    m = json.loads(message)
    if sum([x['user']==m["user"] for x in Q]) < 3:
        Q.append(m)
        if check_fairy_valid(m["code"]):
            all_.append(m["code"])
            with open("states.json", "w") as file:
                json.dump(all_, file)
        print("queued", m)
    else:
        print("trottled", m)

mqttBroker ="localhost"

client = mqtt.Client("mainbot")
client.connect(mqttBroker) 

client.subscribe("chat")
client.on_message=on_message 



try:
    client.loop_start()        
    stream('template2.j2', loop(), (1500,800), 'rtmp://localhost/live/test', fps=10)
finally:
    client.loop_stop()
    client.disconnect()