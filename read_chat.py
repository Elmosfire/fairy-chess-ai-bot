import yaml
from types import SimpleNamespace

import paho.mqtt.client as mqtt 
import socket
from emoji import demojize
import json

sock = socket.socket()

settings = SimpleNamespace(**yaml.load(open("settings.yml"), yaml.SafeLoader))

sock.connect((settings.server, settings.port))

sock.send(f"PASS {settings.token}\n".encode('utf-8'))
sock.send(f"NICK {settings.nickname}\n".encode('utf-8'))
sock.send(f"JOIN {settings.channel}\n".encode('utf-8'))



mqttBroker ="localhost" 

client = mqtt.Client("twitch_listener")
client.connect(mqttBroker)




while True:
    resp = sock.recv(2048).decode('utf-8')

    if resp.startswith('PING'):
        sock.send("PONG\n".encode('utf-8'))
    
    elif len(resp) > 0:
        print(resp)
        message_full = str(demojize(resp))
        if message_full.count(':') >= 2:
            message = message_full.split(':',2)[-1]
        user = message_full.split(':',2)[1].split('!')[0]
        print("message=",message )
        if message.startswith('% '):
            message_string = json.dumps(dict(user=user, code=message.strip()[2:]))
            client.reconnect()
            client.publish("chat", message_string)
            print(f"Just published {message_string} to topic chat")
            
